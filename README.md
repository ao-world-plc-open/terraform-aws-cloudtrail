# Terraform Module: AWS CloudTrail

* [Example Usage](#example-usage)
  * [Basic](#basic)
* [Requirements](#requirements)
* [Inputs](#inputs)
* [Outputs](#outputs)
* [Contributing](#contributing)
* [Change Log](#change-log)

## Example Usage

### Basic

```hcl
module "cloudtrail" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-cloudtrail.git?ref=v1.0.0"
}
```

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | >= 3.26.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| allow\_destroy | Allows the module to be destroyed, defaults to false | `bool` | `false` | no |
| cloud\_watch\_logs\_group\_arn | Specify a log group name ARN that represents the log group to which CloudTrail logs will be delivered | `string` | `""` | no |
| cloud\_watch\_logs\_role\_arn | Specifies the role for the CloudWatch Logs endpoint to assume to write to a user's log group | `string` | `""` | no |
| create\_bucket | Bool indicating whether to create an s3 bucket for trail storage | `string` | `true` | no |
| custom\_tags | A map of tags that will be appended to the tags applied by the module (Name, envtype, envname.). Any tag supplied that matches the name of a tag set by the module will take precedence | `map(any)` | `{}` | no |
| enable\_logging | Bool indicating whether to enable logging | `string` | `true` | no |
| include\_global\_service\_events | Bool indicating whether the trail is publishing events from global services such as IAM to the log files | `string` | `true` | no |
| is\_multi\_region\_trail | Specifies whether the trail is created in the current region or in all regions | `string` | `true` | no |
| kms\_key\_id | Specifies the KMS key ARN to use to encrypt the logs | `string` | `""` | no |
| name | The name of the trail you wish to create | `string` | `"audit-trail"` | no |
| s3\_bucket\_name | The name of the s3 bucket you wish to create for trail storage | `string` | `"cloudtrail-audit"` | no |
| s3\_key\_prefix | The key prefix to use when creating trail storage files | `string` | `"ct-audit"` | no |
| sns\_topic\_name | Specifies the name of the Amazon SNS topic defined for notification of log file delivery | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| cloudtrail\_id | *string* The CloudTrail ID |
| cloudtrail\_s3\_arn | *string* The ARN of the CloudTrail S3 bucket |
| cloudtrail\_s3\_id | *string* The ID of the CloudTrail S3 bucket |

## Contributing

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details on how to contribute to changes to this module.

## Change Log

Please see [CHANGELOG.md](CHANGELOG.md) for changes made between different tag versions of the module.