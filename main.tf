resource "aws_s3_bucket" "audit_trail" {
  bucket = var.s3_bucket_name
  count  = var.create_bucket ? 1 : 0

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSCloudTrailAclCheck",
            "Effect": "Allow",
            "Principal": {"Service": "cloudtrail.amazonaws.com"},
            "Action": "s3:GetBucketAcl",
            "Resource": "arn:aws:s3:::${var.s3_bucket_name}"
        },
        {
            "Sid": "AWSCloudTrailWrite",
            "Effect": "Allow",
            "Principal": {"Service": "cloudtrail.amazonaws.com"},
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::${var.s3_bucket_name}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        }
    ]
}
POLICY

  force_destroy = var.allow_destroy

  tags = merge(map("Name", var.name),
  var.custom_tags)

}

resource "aws_cloudtrail" "audit" {
  name                          = var.name
  enable_logging                = var.enable_logging
  kms_key_id                    = var.kms_key_id
  s3_bucket_name                = var.create_bucket ? element(concat(aws_s3_bucket.audit_trail.*.id, list("")), 0) : var.s3_bucket_name
  s3_key_prefix                 = var.s3_key_prefix
  sns_topic_name                = var.sns_topic_name
  include_global_service_events = var.include_global_service_events
  is_multi_region_trail         = var.is_multi_region_trail
  cloud_watch_logs_group_arn    = var.cloud_watch_logs_group_arn
  cloud_watch_logs_role_arn     = var.cloud_watch_logs_role_arn

  tags = var.custom_tags
}
