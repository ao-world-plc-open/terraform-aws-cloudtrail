output "cloudtrail_id" {
  description = "*string* The CloudTrail ID"
  value       = aws_cloudtrail.audit.id
}

output "cloudtrail_s3_arn" {
  description = "*string* The ARN of the CloudTrail S3 bucket"
  value       = element(concat(aws_s3_bucket.audit_trail.*.arn, list("")), 0)
}

output "cloudtrail_s3_id" {
  description = "*string* The ID of the CloudTrail S3 bucket"
  value       = element(concat(aws_s3_bucket.audit_trail.*.id, list("")), 0)
}

