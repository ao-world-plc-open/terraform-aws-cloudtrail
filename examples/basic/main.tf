module "cloudtrail" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-cloudtrail.git?ref=v1.0.0"
}