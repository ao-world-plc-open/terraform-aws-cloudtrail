variable "allow_destroy" {
  description = "Allows the module to be destroyed, defaults to false"
  default     = false
}

variable "cloud_watch_logs_group_arn" {
  description = "Specify a log group name ARN that represents the log group to which CloudTrail logs will be delivered"
  type        = string
  default     = ""
}

variable "cloud_watch_logs_role_arn" {
  description = "Specifies the role for the CloudWatch Logs endpoint to assume to write to a user's log group"
  type        = string
  default     = ""
}

variable "create_bucket" {
  description = "Bool indicating whether to create an s3 bucket for trail storage"
  type        = string
  default     = true
}

variable "custom_tags" {
  type        = map(any)
  description = "A map of tags that will be appended to the tags applied by the module (Name, envtype, envname.). Any tag supplied that matches the name of a tag set by the module will take precedence"
  default     = {}
}

variable "enable_logging" {
  description = "Bool indicating whether to enable logging"
  type        = string
  default     = true
}

variable "include_global_service_events" {
  description = "Bool indicating whether the trail is publishing events from global services such as IAM to the log files"
  type        = string
  default     = true
}

variable "is_multi_region_trail" {
  description = "Specifies whether the trail is created in the current region or in all regions"
  type        = string
  default     = true
}

variable "kms_key_id" {
  description = "Specifies the KMS key ARN to use to encrypt the logs"
  type        = string
  default     = ""
}

variable "name" {
  description = "The name of the trail you wish to create"
  type        = string
  default     = "audit-trail"
}

variable "s3_bucket_name" {
  description = "The name of the s3 bucket you wish to create for trail storage"
  type        = string
  default     = "cloudtrail-audit"
}

variable "s3_key_prefix" {
  description = "The key prefix to use when creating trail storage files"
  type        = string
  default     = "ct-audit"
}

variable "sns_topic_name" {
  description = "Specifies the name of the Amazon SNS topic defined for notification of log file delivery"
  type        = string
  default     = ""
}
