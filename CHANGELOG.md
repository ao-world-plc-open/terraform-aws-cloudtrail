# Changelog

## v1.0.0 (07/12/2021)

* Ran `terraform fmt` on repository for consistent formatting
* All variables and outputs have been alphabetised
* Added .terraform-docs.yml
* Regenerated README.md with `terraform-docs`
* Added .gitlab-ci.yml to enforce fmt and docs regeneration
* Added .gitignore
* Added LICENCE.md
* Added CONTRIBUTING.md

## v0.3.0 (05/11/2020)

* Upgraded to latest version of Terraform v0.13.4

## v0.2.1 (10/06/2020)

* Readme.md updated with latest tagged version for ease of use.

## v0.2.0 (22/05/2020)

* Adds the ability to specify a custom map of tags using the custom_tags variable.
* Adds the ability to remove the module.

## v0.1.0 (02/09/2017)

IMPROVEMENTS:
* Added functionality to allow Cloudtrail to deliver logs to CloudWatch


## v0.0.5 (24/08/2017)

BUG FIXES:
* Replaced S3 bucket name with bucket ID reference


## v0.0.4 (24/07/2017)

BUG FIXES:
* Reverted change to S3 bucket creation, as Terraform wasn't happy about creating the bucket


## v0.0.3 (24/07/2017)

IMPROVEMENTS:
* Terraform HCL formatted
* Now creates multi-region trails by default
* Now optionally creates S3 bucket, or re-uses existing bucket by name


## v0.0.2 (20/07/2017)

BUG FIXES:
* Restricted S3 bucket policy to Cloudtrail service only


## v0.0.1 (04/07/2016)

Initial version

